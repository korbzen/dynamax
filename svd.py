# -*- coding: utf-8 -*-
"""
Created on Mon Dec 16 14:20:45 2019

@author: Korbzen
This file contains a SVD calculation
"""

import numpy as np

A = np.array([[1,0,1], [-1,1,1], [0,0,1]])

B = np.matmul(A.T, A)

eig, V = np.linalg.eig(B)
eig_s = -np.sort(-eig)

V = gs(V)

sigma = np.identity(A.shape[0]) * eig_s

U = []
for i in range(B.shape[0]):
    u = (1/eig_s[i])*A.dot(V[:,i])
    U.append(u)
    
U = np.array(U)

s = np.matmul(U, sigma)
A_test = np.matmul(s, V.T)



def gs(X, row_vecs=False, norm = True):
    """
    Implements a vectorized Gram-Schmidt
    """
    if not row_vecs:
        X = X.T
    Y = X[0:1,:].copy()
    for i in range(1, X.shape[0]):
        proj = np.diag((X[i,:].dot(Y.T)/np.linalg.norm(Y,axis=1)**2).flat).dot(Y)
        Y = np.vstack((Y, X[i,:] - proj.sum(0)))
    if norm:
        Y = np.diag(1/np.linalg.norm(Y,axis=1)).dot(Y)
    if row_vecs:
        return Y
    else:
        return Y.T
    
test = np.array([[3.0, 1.0], [2.0, 2.0]])
test2 = np.array([[1.0, 1.0, 0.0], [1.0, 3.0, 1.0], [2.0, -1.0, 1.0]])
    