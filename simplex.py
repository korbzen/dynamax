# -*- coding: utf-8 -*-
"""
Created on Wed Dec 18 11:19:57 2019

@author: Korbzen

This is a class implementation of the simplex algorithm.
It is built based on the post by Jacob Moore.
"""

import numpy as np

class SimplexSolver():
    """Simplex Solver
    This class implements a simplex solver.
    """
    def __init__(self, var, constraints):
        """Generatematrix
        This function creates a matrix for the simplex tableau.

        Inputs
        ------
        var: integer
            number of variables
        constraints: integer
            number of contraints

        Returns
        -------
        Initializes the simplex tableau.
        """
        self.tab = np.zeros((constraints+1, var+constraints+2))

        self.len_row = self.tab.shape[0] # pylint: disable=E1136  # pylint/issues/3139
        self.len_col = self.tab.shape[1] # pylint: disable=E1136  # pylint/issues/3139

    def next_round_r(self):
        """Check for pivot
        This functions checks in the right column
        if a pivot is required.

        Attributes
        ----------
        tab: numpy matrix
            this is the initialized matrix

        Returns
        -------
        Boolean
        """
        minima = min(self.tab[:-1, -1])
        return bool(minima <= 0)

    def next_round(self):
        """Check for pivot

        check to see if 1+ pivots are required due to a negative element
         in the bottom row, excluding the final value.

        Parameters
        ----------
        table : numpy array
            DESCRIPTION.

        Returns
        -------
        None.
        """
        minima = min(self.tab[self.len_row-1, :-1])
        if minima >= 0:
            return False
        else:
            return True


    def find_neg_r(self):
        """
        determine where these elements are located.
         start with finding negative elements in the
         furthest right column.
        """
        minima = min(self.tab[:-1, self.len_col-1])
        if minima <= 0:
            n = np.where(self.tab[:-1, self.len_col-1] == minima)[0][0]
        else:
            n = None
        return n

    def find_neg(self):
        """
        locate negative elements in the bottom row
        """
        m = min(self.tab[self.len_row-1, :-1])
        if m <= 0:
            n = np.where(self.tab[self.len_row-1, :-1] == m)[0][0]
        else:
            n = None
        return n

    def loc_piv_r(self):
        """
        """
        total = []
        r = self.find_neg_r()
        row = self.tab[r, :-1]
        m = min(row)
        c = np.where(row == m)[0][0]
        # all elements in column
        col = self.tab[:-1, c]
        # need to go through this column to find smallest positive ratio
        for i, b in zip(col, self.tab[:-1, -1]):
            # i cannot equal 0 and b/i must be positive.
            if i**2 > 0 and b/i > 0:
                total.append(b/i)
            else:
                total.append(0)
        element = max(total)
        for t in total:
            if t > 0 and t < element:
                element = t
        index = total.index(element)
        return [index, c]

    def loc_piv(self):
        """
        """
        if self.next_round():
            total = []
            n = self.find_neg()
            for i, b in zip(self.tab[:-1, n], self.tab[:-1, -1]):
                if i**2 > 0 and b/i > 0:
                    total.append(b/i)
                else:
                    total.append(0)
            element = max(total)
            for t in total:
                if t > 0 and t < element:
                    element = t
            index = total.index(element)
        return [index, n]

    def pivot(self, row, col):
        """
        """
        t = np.zeros((self.len_row, self.len_col))
        pr = self.tab[row, :]
        if self.tab[row, col]**2 > 0:
            e = 1/self.tab[row, col]
            r = pr*e
            for i in range(len(self.tab[:, col])):
                k = self.tab[i, :]
                c = self.tab[i, col]
                if list(k) != list(pr):
                    t[i, :] = list(k-r*c)
            t[row, :] = list(r)
            self.tab = t
        else:
            print('Cannot pivot on this element.')

    def convert(self, eq): # pylint: disable=R0201
        """
        """
        eq = eq.split(',')
        if 'G' in eq:
            g = eq.index('G')
            del eq[g]
            eq = [float(i)*-1 for i in eq]
        if 'L' in eq:
            l = eq.index('L')
            del eq[l]
            eq = [float(i) for i in eq]
        return eq

    def convert_min(self):
        """
        """
        self.tab[-1, :-2] = [-1*i for i in self.tab[-1, :-2]]
        self.tab[-1, -1] = -1*self.tab[-1, -1]
        return self.tab

    def gen_var(self):
        """
        """
        lc = len(self.tab[0, :])
        lr = len(self.tab[:, 0])
        var = lc - lr -1
        v = []
        for i in range(var):
            v.append('x'+str(i+1))
        return v

    def add_cons(self):
        """
        """
        lr = len(self.tab[:, 0])
        empty = []
        for i in range(lr):
            total = 0
            for j in self.tab[i, :]:
                total += j**2
            if total == 0:
                empty.append(total)
        return bool(len(empty) > 1)

    def constrain(self, eq):
        """
        """
        if self.add_cons() == True:
            lc = len(self.tab[0, :])
            lr = len(self.tab[:, 0])
            var = lc - lr -1
            j = 0
            while j < lr:
                row_check = self.tab[j, :]
                total = 0
                for i in row_check:
                    total += float(i**2)
                if total == 0:
                    row = row_check
                    break
                j += 1
            eq = self.convert(eq)
            i = 0
            while i < len(eq)-1:
                row[i] = eq[i]
                i += 1
            row[-1] = eq[-1]
            row[var+j] = 1
        else:
            print('Cannot add another constraint.')

    def add_obj(self):
        """
        """
        lr = len(self.tab[:, 0])
        empty = []
        for i in range(lr):
            total = 0
            for j in self.tab[i, :]:
                total += j**2
            if total == 0:
                empty.append(total)
        return bool(len(empty) == 1)

    def obj(self, eq):
        """
        """
        if self.add_obj() == True:
            eq = [float(i) for i in eq.split(',')]
            lr = len(self.tab[:, 0])
            row = self.tab[lr-1, :]
            i = 0
            while i < len(eq)-1:
                row[i] = eq[i]*-1
                i += 1
            row[-2] = 1
            row[-1] = eq[-1]
        else:
            print('You must finish adding constraints before the objective function can be added.')

    def maxz(self):
        """
        """
        while self.next_round_r() == True:
            self.pivot(self.loc_piv_r()[0],
                       self.loc_piv_r()[1])
        while self.next_round() == True:
            self.pivot(self.loc_piv()[0],
                       self.loc_piv()[1])
        var = self.len_col - self.len_row -1
        i = 0
        val = {}
        for i in range(var):
            col = self.tab[:, i]
            colsum = sum(col)
            m = max(col)
            if float(colsum) == float(m):
                loc = np.where(col == m)[0][0]
                val[self.gen_var()[i]] = self.tab[loc, -1]
            else:
                val[self.gen_var()[i]] = 0
        val['max'] = self.tab[-1, -1]
        return val

    def minz(self):
        """
        """
        self.convert_min()
        while self.next_round_r() == True:
            self.pivot(self.loc_piv_r()[0],
                       self.loc_piv_r()[1])
        while self.next_round() == True:
            self.pivot(self.loc_piv()[0],
                       self.loc_piv()[1])
        lc = len(self.tab[0, :])
        lr = len(self.tab[:, 0])
        var = lc - lr -1
        i = 0
        val = {}
        for i in range(var):
            col = self.tab[:, i]
            colsum = sum(col)
            m = max(col)
            if float(colsum) == float(m):
                loc = np.where(col == m)[0][0]
                val[self.gen_var()[i]] = self.tab[loc, -1]
            else:
                val[self.gen_var()[i]] = 0
        val['min'] = self.tab[-1, -1]*-1
        return val

if __name__ == "__main__":
    s = SimplexSolver(2, 2)
    s.constrain('2,-1,G,10')
    s.constrain('1,1,L,20')
    s.obj('5,10,0')
    print(s.maxz())
    s = SimplexSolver(2, 4)
    s.constrain('2,5,G,30')
    s.constrain('-3,5,G,5')
    s.constrain('8,3,L,85')
    s.constrain('-9,7,L,42')
    s.obj('2,7,0')
    print(s.minz())

    s = SimplexSolver(2, 4)
    s.constrain('1,1,L,18')
    s.constrain('1,0,L,11')
    s.constrain('0,1,L,10')
    s.constrain('1,1,G,9')
    s.obj('-1,-3, 229')
    print(s.minz())
